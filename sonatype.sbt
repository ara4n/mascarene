import xerial.sbt.Sonatype._
import sbt.url

ThisBuild / publishMavenStyle := true

ThisBuild / sonatypeProfileName := "org.mascarene"
ThisBuild / sonatypeProjectHosting := Some(
  GitLabHosting(user = "mascarene", repository = "mascarene", email = "nico@beerfactory.org")
)
ThisBuild / developers := List(
  Developer(
    id = "NicolasJouanin",
    name = "Nicolas Jouanin",
    email = "nico@beerfactory.org",
    url = url("https://gitlab.com/NicolasJouanin")
  )
)
ThisBuild / licenses := Seq("AGPL-V3" -> url("https://www.gnu.org/licenses/agpl-3.0.html"))

ThisBuild / publishTo := sonatypePublishToBundle.value
/*
ThisBuild / publishTo := {
  val nexus = "https://oss.sonatype.org/"
  if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
  else Some("releases" at nexus + "service/local/staging/deploy/maven2")
}
 */
