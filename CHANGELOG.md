# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- implementation for `GET /_matrix/client/versions` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-versions)) 
- implementation for `GET /.well-known/matrix/client` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-well-known-matrix-client)) 
- implementation for `GET /_matrix/client/r0/login` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-login))
- implementation for `POST /_matrix/client/r0/login` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-login))
- implementation for `POST /_matrix/client/r0/logout` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-logout))
- implementation for `POST /_matrix/client/r0/logout/all` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-logout-all))
- implementation for `POST /_matrix/client/r0/register` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-register))
- implementation for `GET /_matrix/client/r0/account/whoami` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-account-whoami))
- implementation for `POST /_matrix/client/r0/user/{userId}/filter` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-user-userid-filter))
- implementation for `GET /_matrix/client/r0/user/{userId}/filter/{filterId}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-user-userid-filter-filterid))
- implementation for `GET /_matrix/client/r0/sync` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-sync)) **Partially implemented**: ephemeral, presence and filtering are not implemented
- implementation for `POST /_matrix/client/r0/createRoom` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#post-matrix-client-r0-createroom))
- implementation for `PUT /_matrix/client/r0/rooms/{roomId}/send/{eventType}/{txnId}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-user-userid-account-data-type))
- implementation for `PUT /_matrix/client/r0/user/{userId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-user-userid-account-data-type))
- implementation for `GET /_matrix/client/r0/user/{userId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-user-userid-account-data-type))
- implementation for `PUT /_matrix/client/r0/user/{userId}/rooms/{roomId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#put-matrix-client-r0-user-userid-rooms-roomid-account-data-type))
- implementation for `GET /_matrix/client/r0/user/{userId}/rooms/{roomId}/account_data/{type}` client API endpoint ([r0.6.0](https://matrix.org/docs/spec/client_server/r0.6.0#get-matrix-client-r0-user-userid-rooms-roomid-account-data-type))

### Changed
None

### Removed
None