/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.matrix.client.r0

import org.mascarene.matrix.client.r0.model.auth.Flow

class AuthFlowException(
    message: String,
    completed: Option[List[String]] = None,
    errcode: Option[String] = None,
    error: Option[String] = None,
    flows: List[Flow],
    params: Map[String, Map[String, String]] = Map.empty,
    session: Option[String] = None
) extends Throwable(message)
