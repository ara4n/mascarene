# Installing Mascarene

## Installing from fat JAR

This installation consists in installing and runnning the JAR provided by mascarene and which containts the application 
compiled code with all its dependencies. 

This installation requires you to provide the following requirements:
 * POSIX-compliant system
 * PostgreSQL 11 or above
 * Redis 6 or above
 
Before installing Mascarene, you need to prepare the database environment. Depending on your choise you can create a new 
database or create a specific schema into an existing database. Whatever you choose, we recommend you to create a 
database user specifically for mascarene.

```
$ psql
postgres=# CREATE USER mascarene WITH ENCRYPTED PASSWORD 'some_password';
postgres=# CREATE DATABASE mascarene OWNER mascarene;
```   

Once the database is up and running, we recommend creating a system user for installing and running mascarene. Let's say 
you've created a `mascarene` user for this purpose. 
 * Download mascarene latest release package from *TODO: provide a place to download native packages*
 * un-archive the downloaded archive into the installation directory
 
 ```
$ mkdir -p ~/mascarene
$ wget https://gitlab.com/mascarene/mascarene/-/jobs/564910990/artifacts/raw/homeserver/target/universal/mascarene-homeserver-0.0.1-SNAPSHOT.tgz?inline=false
$ tar xzvf mascarene-homeserver-0.0.1-SNAPSHOT.tgz
$ cd mascarene-homeserver-0.0.1-SNAPSHOT
```  

You can run mascarene with the provided script `./bin/mascarene-main`, but before **you must configure it**.


## Installing from Docker image

