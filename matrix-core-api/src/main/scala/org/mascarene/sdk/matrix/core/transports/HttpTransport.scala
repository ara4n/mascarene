/*
Copyright 2018 Nicolas Jouanin

This file is part of Plasma.

Plasma is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License 3.

Plasma is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>
 */
package org.mascarene.sdk.matrix.core.transports

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.OAuth2BearerToken
import akka.stream.Materializer
import akka.util.ByteString
import com.typesafe.scalalogging.LazyLogging
import io.circe.{Decoder, Encoder, Printer}
import io.circe.parser._
import io.circe.syntax._
import org.mascarene.sdk.matrix.core.Transport
import org.mascarene.sdk.matrix.core.model.AuthToken

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.util.{Failure, Success}

trait HttpTransport extends Transport with LazyLogging {
  implicit def system: ActorSystem
  implicit def materializer: Materializer
  implicit def executionContext: ExecutionContextExecutor

  private val jsonPrinter = Printer.noSpaces.copy(dropNullValues = true)
  private val httpClient  = Http()

  protected def responseDecodeHandler[R](entity: String)(implicit decoder: Decoder[R]): R

  protected def doPut[Q, R](
      uri: Uri,
      request: Q,
      queries: Map[String, String] = Map.empty
  )(implicit encoder: Encoder[Q], decoder: Decoder[R], token: Option[AuthToken] = None): Future[R] =
    doHttpCall(
      HttpRequest(
        method = HttpMethods.PUT,
        uri = uri.withQuery(Uri.Query(queries)),
        entity = HttpEntity(MediaTypes.`application/json`, jsonPrinter.pretty(request.asJson)),
        headers = token match {
          case Some(t) => List(headers.Authorization(OAuth2BearerToken(t.value)))
          case None    => List.empty
        }
      )
    )

  protected def doPost[Q, R](
      uri: Uri,
      request: Q,
      queries: Map[String, String] = Map.empty
  )(implicit encoder: Encoder[Q], decoder: Decoder[R], token: Option[AuthToken] = None): Future[R] =
    doHttpCall(
      HttpRequest(
        method = HttpMethods.POST,
        uri = uri.withQuery(Uri.Query(queries)),
        entity = HttpEntity(MediaTypes.`application/json`, jsonPrinter.pretty(request.asJson)),
        headers = token match {
          case Some(t) => List(headers.Authorization(OAuth2BearerToken(t.value)))
          case None    => List.empty
        }
      )
    )

  /**
    * Perform an HTTP GET and returns either a server response object (Right)
    * or an error (Left) which can be a ServerError (server side) or a ClientError (client side)
    * @param uri
    * @param decoder
    * @tparam R Type of the object encoded returned json
    * @return
    */
  protected def doGet[R](
      uri: Uri,
      queries: Map[String, String] = Map.empty
  )(implicit decoder: Decoder[R], token: Option[AuthToken] = None): Future[R] =
    doHttpCall(
      HttpRequest(
        method = HttpMethods.GET,
        uri = uri.withQuery(Uri.Query(queries)),
        entity = HttpEntity.empty(MediaTypes.`application/json`),
        headers = token match {
          case Some(t) => List(headers.Authorization(OAuth2BearerToken(t.value)))
          case None    => List.empty
        }
      )
    )

  protected def doDelete[R](
      uri: Uri,
      queries: Map[String, String] = Map.empty
  )(implicit decoder: Decoder[R], token: Option[AuthToken] = None): Future[R] =
    doHttpCall(
      HttpRequest(
        method = HttpMethods.DELETE,
        uri = uri.withQuery(Uri.Query(queries)),
        entity = HttpEntity.empty(MediaTypes.`application/json`),
        headers = token match {
          case Some(t) => List(headers.Authorization(OAuth2BearerToken(t.value)))
          case None    => List.empty
        }
      )
    )

  private def doHttpCall[R](request: HttpRequest)(implicit decoder: Decoder[R]): Future[R] = {
    logRequest(request)
    httpClient
      .singleRequest(request)
      .flatMap { response =>
        val decodeFuture = response.entity.dataBytes
          .runFold(ByteString(""))(_ ++ _)
          .map(body => responseDecodeHandler(body.utf8String))
        logger.whenDebugEnabled {
          decodeFuture
            .onComplete {
              case Success(payload) =>
                logger.debug(s"${request.method.value} ${request.uri} <- ${response.status} : $payload")
              case Failure(t) =>
                logger.debug(s"${request.method.value} ${request.uri} <- ${response.status} : ${t.getMessage}")
            }
        }
        decodeFuture
      }
  }

  private def logRequest(request: HttpRequest): Unit = logger.whenDebugEnabled {
    request.entity.dataBytes
      .runFold(ByteString(""))(_ ++ _)
      .map(_.utf8String)
      .onComplete {
        case Success(payload) => logger.debug(s"${request.method.value} ${request.uri} -> $payload")
        case _                => logger.debug(s"${request.method.value} ${request.uri}")
      }
  }
}
