/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.util.UUID

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import org.mascarene.homeserver.server.model.{AuthRepo, DbContext, User}
import redis.clients.jedis.JedisPool
import scalacache._
import scalacache.modes.try_._
import scalacache.redis._
import scalacache.serialization.circe._

import scala.jdk.DurationConverters._
import scala.util.{Success, Try}

class AuthCache(config: Config, dbContext: DbContext, jedisPool: JedisPool) extends LazyLogging {
  private[this] val authRepo                = new AuthRepo(dbContext)
  private[this] val usersCache: Cache[User] = RedisCache(jedisPool)
  private[this] val userCacheTtl            = Some(config.getDuration("mascarene.internal.cache-ttl.user").toScala)

  private def userKey(userId: UUID) = s"user($userId)"
  private def userKey(mxId: String) = s"user($mxId)"

  def getUser(userId: UUID): Try[Option[User]] = {
    usersCache.get(userKey(userId)).flatMap {
      case Some(user) =>
        Success(Some(user))
      case None =>
        authRepo.getUserById(userId).map { maybeUser =>
          maybeUser.map { user =>
            usersCache.put(userKey(userId))(user, userCacheTtl)
            user
          }
        }
    }
  }

  def getUser(mxId: String): Try[Option[User]] = {
    usersCache.get(userKey(mxId)).flatMap {
      case Some(user) =>
        Success(Some(user))
      case None =>
        authRepo.getUserByMxId(mxId).map { maybeUser =>
          maybeUser.map { user =>
            usersCache.put(userKey(mxId))(user, userCacheTtl)
            user
          }
        }
    }
  }
}
