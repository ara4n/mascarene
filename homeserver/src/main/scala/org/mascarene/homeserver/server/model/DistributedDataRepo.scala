/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import io.circe._
import io.circe.parser._
import io.circe.syntax._
import io.circe.generic.auto._

import scala.util.Try

case class DistributedData(key: String, value: Json)

class DistributedDataRepo(val dbContext: DbContext) extends JsonCodec {
  import dbContext._

  private val distributedDatas = quote(querySchema[DistributedData]("distributed_data"))

  def getValue(key: String): Try[Option[Json]] = Try {
    run(distributedDatas.filter(_.key == lift(key)).map(_.value)).headOption
  }

  def putValue(key: String, value: Json): Try[Long] = Try {
    val data = DistributedData(key, value)
    run(distributedDatas.insert(lift(data)).onConflictUpdate(_.key)((t, e) => t.value -> e.value))
  }
}
