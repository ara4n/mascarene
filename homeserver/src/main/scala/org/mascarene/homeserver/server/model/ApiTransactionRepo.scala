package org.mascarene.homeserver.server.model

import java.time.LocalDateTime

import io.circe.Json

import scala.util.Try

case class ApiTransaction(path: String, responseContent: Json, createAt: LocalDateTime)

class ApiTransactionRepo(val dbContext: DbContext) extends JsonCodec {
  import dbContext._

  private val apiTransactions = quote(querySchema[ApiTransaction]("api_transaction"))

  def storeApiTransaction(path: String, content: Json): Try[ApiTransaction] = Try {
    val newApiTransaction = ApiTransaction(path, content, LocalDateTime.now())
    run {
      apiTransactions.insert(lift(newApiTransaction))
    }
    newApiTransaction
  }

  def getApiTransaction(path: String): Try[Option[ApiTransaction]] = Try {
    run {
      apiTransactions.filter(_.path == lift(path))
    }.headOption
  }

}
