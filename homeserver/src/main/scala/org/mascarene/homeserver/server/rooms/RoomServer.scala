/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.time.{Instant, LocalDateTime}
import java.util.UUID

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors, StashBuffer}
import akka.actor.typed.{ActorRef, ActorSystem, Behavior, PostStop, Signal}
import akka.cluster.sharding.typed.scaladsl.EntityTypeKey
import com.typesafe.config.Config
import io.circe.Json
import io.circe.generic.auto._
import io.circe.syntax._
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.server.model._
import org.mascarene.homeserver.server.rooms.RoomStateResolver.{ResolveEvent, StateResolved}
import org.mascarene.sdk.matrix.core.ApiFailure
import org.mascarene.utils.Codecs
import redis.clients.jedis.JedisPool
import akka.actor.typed.scaladsl.adapter._
import akka.stream.{OverflowStrategy, QueueOfferResult}
import akka.stream.scaladsl.Source
import akka.stream.typed.scaladsl.{ActorFlow, ActorSink}
import org.mascarene.homeserver.matrix.server.PDU
import org.mascarene.homeserver.server.rooms.EventStreamSequence.{EventStreamSequenceValue, NextVal}
import org.mascarene.homeserver.server.rooms.RoomWorker.CastStateEventEffects

import scala.concurrent.Await
import scala.jdk.DurationConverters._
import scala.util.{Failure, Success, Try}

object RoomServer {
  val TypeKey: EntityTypeKey[RoomServer.Command] = EntityTypeKey[RoomServer.Command]("RoomServer")
  sealed trait Response
  case class RoomStateResponse(roomState: RoomState) extends Response
  case class RoomEvent(event: Try[Event])            extends Response

  sealed trait Command
  case class InitialState(room: Room)                           extends Command
  case class GetRoomState(replyTo: ActorRef[RoomStateResponse]) extends Command
  case class SetVisibility(visibility: String)                  extends Command
  case class PostEvent(
      eventType: String,
      stateKey: Option[String],
      senderId: UUID,
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty,
      replyTo: Option[ActorRef[RoomEvent]]
  ) extends Command
  case class InsertEventWithSequenceOrder(
      eventType: String,
      stateKey: Option[String],
      senderId: UUID,
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty,
      streamOrderNextVal: Long,
      replyTo: Option[ActorRef[RoomEvent]]
  ) extends Command
  case class WrappedReply[A <: Response](r: A, replyTo: ActorRef[A]) extends Command
  case class WrappedQueueOfferResult(response: QueueOfferResult)     extends Command
  case class WrappedQueueOfferFailure(ex: Throwable)                 extends Command
  case class UpdateState(event: Event, stateSet: Try[StateSet])      extends Command
  case class ResolveQueueTermination(ex: Option[Throwable])          extends Command
  case object Recover                                                extends Command

  def apply(
      entityId: String,
      config: Config,
      dbContext: DbContext,
      jedisPool: JedisPool,
      eventStreamSequence: ActorRef[EventStreamSequence.Command],
      workerRouter: ActorRef[RoomWorker.Command]
  ): Behavior[RoomServer.Command] =
    Behaviors.setup { ctx =>
      new RoomServer(entityId, config, ctx, dbContext, jedisPool, eventStreamSequence, workerRouter)
    }
}

class RoomServer(
    roomId: String,
    val config: Config,
    context: ActorContext[RoomServer.Command],
    dbContext: DbContext,
    jedisPool: JedisPool,
    eventStreamSequence: ActorRef[EventStreamSequence.Command],
    workerRouter: ActorRef[RoomWorker.Command]
) extends AbstractBehavior[RoomServer.Command](context)
    with ImplicitAskTimeOut {
  import RoomServer._
  implicit val system: ActorSystem[Nothing] = context.system
  private val roomRepo                      = new RoomRepo(dbContext)
  private val eventRepo                     = new EventRepo(dbContext)
  private val authCache                     = new AuthCache(config, dbContext, jedisPool)
  private val stateSetCache                 = new StateSetCache(config, dbContext, jedisPool)
  private val stateSetApi                   = new StateSetApi(config, dbContext)

  private[this] val resolutionQueueSize    = config.getInt("mascarene.internal.server.room.resolution-queue-size")
  private[this] val enqueueWaitStashBuffer = config.getInt("mascarene.internal.server.room.enqueue-wait-stash-buffer")
  private[this] val defaultAwaitTimeout    = config.getDuration("mascarene.internal.default-await-timeout").toScala

  private[this] val matrixServerName = config.getString("mascarene.server.domain-name")
  private[this] val room: Room = roomRepo.getRoomById(UUID.fromString(roomId)) match {
    case Success(Some(result)) => result
    case _ =>
      throw new ApiFailure("IO.MASCARENE.HS.INTERNAL_ERROR", s"Can't start room server for unknown room $roomId")
  }
  private[this] var roomState   = RoomState(room)
  private val roomStateResolver = context.spawn(RoomStateResolver(config, dbContext, jedisPool, room), "stateResolver")

  private[this] val resolveQueue = Source
    .queue[Event](resolutionQueueSize, OverflowStrategy.backpressure)
    .via(
      ActorFlow.ask[Event, ResolveEvent, StateResolved](roomStateResolver)((event, replyTo) =>
        ResolveEvent(replyTo, event)
      )
    )
    .map(stateResolved => UpdateState(stateResolved.updatedEvent, stateResolved.stateSet))
    .to(
      ActorSink.actorRef[Command](context.self, ResolveQueueTermination(None), ex => ResolveQueueTermination(Some(ex)))
    )
    .run()

  // Recover event resolution on startup
  eventRepo.getUnresolvedEvents(roomState.room.id).map { events =>
    val nbRecovered = events.map { event =>
      context.log.debug(s"Recovering event $event resolution")
      Await.result(resolveQueue.offer(event), defaultAwaitTimeout)
      1
    }.sum
    context.log.info(s"Room=${roomState.room.mxRoomId}: $nbRecovered events recovered for resolution")
  }
  eventRepo.getUnProcessedEvents(roomState.room.id).map { events =>
    val nbRecovered = events.map { event =>
      context.log.debug(s"Recovering event $event process")
      workerRouter ! CastStateEventEffects(roomState, event)
      1
    }.sum
    context.log.info(s"Room=${roomState.room.mxRoomId}: $nbRecovered events recovered for side effect process")
  }

  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case GetRoomState(replyTo) =>
        replyTo ! RoomStateResponse(roomState)
        Behaviors.same
      case UpdateState(resolvedEvent, stateSetTry) =>
        stateSetTry
          .map { resolvedStateSet =>
            if (!resolvedEvent.rejected && resolvedEvent.isStateEvent) {
              // Create a new room state version
              // and update room state
              val previousState = roomState.resolvedState
              roomState = roomState.copy(lastResolvedEvent = Some(resolvedEvent), resolvedState = resolvedStateSet)
              val updateRoomTry = for {
                newStateSetVersion <- stateSetApi.createNewVersion(roomState.room, resolvedStateSet, previousState)
                updatedRoom        <- roomRepo.updateLastStateSetVersion(roomState.room.id, newStateSetVersion)
              } yield updatedRoom
              updateRoomTry match {
                case Success(updatedRoom) =>
                  roomState = roomState.copy(room = updatedRoom)
                  context.log.debug(
                    s"room ${room.mxRoomId} state updated to lastEvent=${roomState.lastResolvedEvent.get.mxEventId}"
                  )
                  // Cast state change side effects
                  workerRouter ! CastStateEventEffects(roomState, resolvedEvent)
                case Failure(f) => context.log.warn(s"Room state version couldn't be persisted: ${f.getMessage}")
              }
            } else {
              eventRepo.updateEventProcessed(resolvedEvent.id, processed = true)
            }
          }
          .recover(f => context.log.warn(s"Event resolution failed for event $resolvedEvent: ${f.getMessage}"))
        //Event was rejected, associate its state to the current room state
        stateSetCache.put(resolvedEvent.id, roomState.resolvedState)
        eventRepo.updateEventStateSetVersion(Set(resolvedEvent.id), roomState.room.lastStatesetVersion)
        Behaviors.same
      case ResolveQueueTermination(cause) =>
        cause.foreach(f => context.log.warn(s"resolution queue terminated on error: ${f.getMessage}"))
        Behaviors.same
      case pe: PostEvent =>
        val parentsId: Set[UUID] = if (pe.parentsId.isEmpty && roomState.lastReceivedEvent.isDefined) {
          Set(roomState.lastResolvedEvent.map(_.id).getOrElse(roomState.lastReceivedEvent.get.id))
        } else {
          pe.parentsId
        }
        context.ask[NextVal, EventStreamSequenceValue](eventStreamSequence, NextVal.apply) {
          case Success(EventStreamSequenceValue(nextval)) =>
            context.log.debug(
              s"Event posted to room ${room.mxRoomId} : (type=${pe.eventType}, stateKey=${pe.stateKey}, sender=${pe.senderId}, streamOrder=$nextval"
            )
            InsertEventWithSequenceOrder(
              pe.eventType,
              pe.stateKey,
              pe.senderId,
              pe.content,
              parentsId,
              pe.authsEventId,
              nextval,
              pe.replyTo
            )
          case Failure(f) => WrappedReply(RoomEvent(Failure(f)), pe.replyTo.get)
        }
        Behaviors.same
      case WrappedReply(reply, replyTo: ActorRef[Response]) =>
        replyTo ! reply
        Behaviors.same
      case InsertEventWithSequenceOrder(
          eventType,
          stateKey,
          senderId,
          content,
          parentsId,
          authsEventId,
          nextval,
          replyTo
          ) =>
        val newEvent =
          insertEvent(roomState.room, eventType, stateKey, senderId, content, parentsId, authsEventId, nextval)

        // go to process event state if the created event is a state event
        newEvent match {
          case Success(event) =>
            pushEventToResolution(event)
          case Failure(f) =>
            context.log.error("Couldn't insert new event", f)
        }
        replyTo.foreach(replyTo => replyTo ! RoomEvent(newEvent))
        context.log.debug("Waiting event enqueued to resolution queue")
        Behaviors.withStash(enqueueWaitStashBuffer) { buffer => waitEnqueue(buffer) }
    }
  }
  private def pushEventToResolution(event: Event) = {
    val offerResult = resolveQueue.offer(event)
    context.log.debug(s"Event ${event.id} pushed to resolution queue")
    context.pipeToSelf(offerResult) {
      case Success(result) => WrappedQueueOfferResult(result)
      case Failure(f)      => WrappedQueueOfferFailure(f)
    }
    roomState = roomState.copy(lastReceivedEvent = Some(event))
  }

  private def waitEnqueue(buffer: StashBuffer[Command]): Behavior[Command] = Behaviors.receiveMessage {
    case WrappedQueueOfferFailure(f) =>
      context.log
        .error("new room event couldn't be pushed to server resolution queue, event will need recovery", f)
      buffer.unstashAll(this)
    case WrappedQueueOfferResult(response) =>
      context.log.debug(s"queue offer result: $response")
      context.log.debug("Finished waiting enqueue")
      buffer.unstashAll(this)
    case other =>
      // stash all other messages for later processing
      buffer.stash(other)
      Behaviors.same
  }

  override def onSignal: PartialFunction[Signal, Behavior[Command]] = {
    case PostStop =>
      context.log.debug("Wait resolve queue completion before stopping")
      Try {
        resolveQueue.complete()
        Await.result(resolveQueue.watchCompletion(), defaultAwaitTimeout)
      }
      context.log.debug("Resolve queue completed")
      Behaviors.same
  }

  private def insertEvent(
      room: Room,
      eventType: String,
      stateKey: Option[String],
      senderId: UUID,
      content: Option[Json],
      parentsId: Set[UUID] = Set.empty,
      authsEventId: Set[UUID] = Set.empty,
      streamOrderNextVal: Long
  ): Try[Event] = {
    val now = Instant.now()
    for {
      senderMxId  <- authCache.getUser(senderId).map(_.get)
      parentsMxId <- eventRepo.getEventsByIds(parentsId).map(events => events.map(_.mxEventId))
      authsMxId   <- eventRepo.getEventsByIds(authsEventId).map(events => events.map(_.mxEventId))
      eventMxId <- Try {
        generateMxEventId(
          roomState.room,
          now,
          eventType,
          stateKey,
          senderMxId.mxUserId,
          content.getOrElse(Json.Null),
          parentsMxId,
          authsMxId
        )
      }
      event <- eventRepo.insertEvent(
        roomId = room.id,
        senderId = senderId,
        eventType = eventType,
        stateKey = stateKey,
        content = content,
        parentsId = parentsId,
        authsEventId = authsEventId,
        originServerTs = LocalDateTime.now(),
        mxEventId = eventMxId,
        streamOrder = streamOrderNextVal
      )
    } yield event
  }

  private def generateMxEventId(
      room: Room,
      originServerTs: Instant,
      eventType: String,
      stateKey: Option[String],
      senderMxId: String,
      content: Json,
      prevEventsMxId: Set[String],
      authEventsMxId: Set[String],
      redacts: Option[String] = None
  ): String = {

    room.version match {
      case "1" | "2" =>
        s"$$${Codecs.genId()}:$matrixServerName"
      case "3" | "4" | "5" =>
        val eventHash = computeEventHash(
          room.mxRoomId,
          originServerTs,
          eventType,
          stateKey,
          senderMxId,
          content,
          prevEventsMxId,
          authEventsMxId,
          redacts
        )
        s"$$${Codecs.toBase64(eventHash, unpadded = true)}"
      case _ => throw new ApiFailure("M_UNSUPPORTED_ROOM_VERSION", s"Unsupported room version '${room.version}'")
    }
  }

  private def computeEventHash(
      roomMxId: String,
      originServerTs: Instant,
      eventType: String,
      stateKey: Option[String],
      senderMxId: String,
      content: Json,
      prevEventsMxId: Set[String],
      authEventsMxId: Set[String],
      redacts: Option[String] = None
  ): Array[Byte] = {
    val pdu =
      PDU(
        roomMxId,
        senderMxId,
        matrixServerName,
        originServerTs.toEpochMilli,
        eventType,
        stateKey,
        content,
        prevEventsMxId,
        0,
        authEventsMxId,
        redacts,
        None,
        Map.empty,
        Map.empty
      )
    val modifiedJson =
      pdu.asJson.hcursor.downField("unsigned").delete.downField("signatures").delete.downField("hashes").delete.top.get
    val jsonString = modifiedJson.dropNullValues.noSpacesSortKeys
    Codecs.sha256(jsonString)
  }
}
