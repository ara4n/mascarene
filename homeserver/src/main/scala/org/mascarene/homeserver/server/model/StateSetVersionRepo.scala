/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.time.LocalDateTime
import java.util.UUID

import scala.util.Try

case class StateSetVersion(roomId: UUID, version: Long, stateEventId: UUID)

class StateSetVersionRepo(dbContext: DbContext) {
  import dbContext._

  private val stateSetVersions = quote(querySchema[StateSetVersion]("stateset_versions"))
  private val events           = quote(querySchema[Event]("events"))

  def createVersion(roomId: UUID, eventsId: Set[UUID]): Try[Long] = {
    transaction {
      for {
        version <- nextVersion(roomId)
        _       <- insertStateSetVersion(roomId, version, eventsId)
      } yield version
    }
  }

  private def insertStateSetVersion(roomId: UUID, version: Long, eventsId: Set[UUID]): Try[Set[StateSetVersion]] = Try {
    val now       = LocalDateTime.now()
    val stateSets = eventsId.map(eventId => StateSetVersion(roomId, version, eventId))
    run {
      quote {
        liftQuery(stateSets).foreach { stateSetVersion => stateSetVersions.insert(stateSetVersion) }
      }
    }
    stateSets
  }

  private def nextVersion(roomId: UUID): Try[Long] = Try {
    run(stateSetVersions.filter(_.roomId == lift(roomId)).map(_.version).max).map(_ + 1).getOrElse(0L)
  }

  def getLastVersionStateEvents(roomId: UUID): Try[Set[UUID]] =
    Try {
      run { stateSetVersions.filter(_.roomId == lift(roomId)).map(_.version).max }.getOrElse(-1L)
    }.flatMap(lastVersion => getVersionStateEvents(roomId, lastVersion))

  /**
    * Load all state events participating in the given state set version
    * @param roomId
    * @param version
    * @return
    */
  def getVersionStateEvents(roomId: UUID, version: Long): Try[Set[UUID]] = Try {
    run {
      stateSetVersions
        .filter(_.roomId == lift(roomId))
        .filter(_.version == lift(version))
    }.map(_.stateEventId).toSet
  }

}
