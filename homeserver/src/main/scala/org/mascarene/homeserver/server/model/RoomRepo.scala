/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.model

import java.time.LocalDateTime
import java.util.UUID

import io.circe._
import io.circe.generic.semiauto._
import org.mascarene.matrix.client.r0.model.events.UnsignedData

import scala.concurrent.Future
import scala.util.Try

case class Room(
    id: UUID,
    mxRoomId: String,
    visibility: String,
    version: String,
    lastStatesetVersion: Long,
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

case class RoomMembership(
    roomId: UUID,
    userId: UUID,
    eventId: UUID,
    membership: String,
    createdAt: LocalDateTime,
    updatedAt: Option[LocalDateTime]
)

class RoomRepo(val dbContext: DbContext) extends JsonCodec {
  import dbContext._

  private val rooms           = quote(querySchema[Room]("rooms"))
  private val roomMemberships = quote(querySchema[RoomMembership]("room_memberships"))
  private val events          = quote(querySchema[Event]("events"))
  private val users           = quote(querySchema[User]("users"))

  def getRoomByMxId(mxRoomId: String): Try[Option[Room]] = Try {
    run { rooms.filter(_.mxRoomId == lift(mxRoomId)) }.headOption
  }

  def getRoomById(id: UUID): Try[Option[Room]] = Try {
    run { rooms.filter(_.id == lift(id)) }.headOption
  }

  def createRoom(mxRoomId: String, visibility: String, version: String): Try[Room] = Try {
    val newRow = Room(UUID.randomUUID(), mxRoomId, visibility, version, -1, LocalDateTime.now(), None)
    run { rooms.insert(lift(newRow)) }
    newRow
  }

  def updateLastStateSetVersion(roomId: UUID, lastVersion: Long): Try[Room] = {
    val now = LocalDateTime.now()
    getRoomById(roomId).flatMap {
      case Some(room) =>
        Try {
          run(
            rooms
              .filter(_.id == lift(roomId))
              .update(_.lastStatesetVersion -> lift(lastVersion), _.updatedAt -> lift(Some(now): Option[LocalDateTime]))
          )
        }.map(_ => room.copy(lastStatesetVersion = lastVersion, updatedAt = Some(now)))
    }
  }

  def createOrUpdateMembership(roomId: UUID, userId: UUID, eventId: UUID, membership: String): Try[RoomMembership] =
    Try {
      val now           = LocalDateTime.now()
      val newMembership = RoomMembership(roomId, userId, eventId, membership, now, None)
      val updatedAt = run {
        roomMemberships
          .insert(lift(newMembership))
          .onConflictUpdate(_.roomId, _.userId)(
            (t, e) => t.membership -> e.membership,
            (t, e) => t.eventId    -> e.eventId,
            (t, e) => t.updatedAt  -> Some(e.createdAt)
          )
          .returning(r => (r.eventId, r.updatedAt))
      }
      newMembership.copy(eventId = updatedAt._1, updatedAt = updatedAt._2)
    }

  def getRoomMemberships(userId: UUID): Try[Iterable[(RoomMembership, Room, Event)]] = Try {
    run {
      roomMemberships
        .filter(_.userId == lift(userId))
        .join(rooms)
        .on(_.roomId == _.id)
        .join(events)
        .on { case ((rm, _), e) => rm.eventId == e.id }
        .map { case ((rm, m), e) => (rm, m, e) }
    }
  }

  def getRoomMembers(roomId: UUID, membership: String): Try[Iterable[(User, Event)]] = Try {
    run {
      roomMemberships
        .filter(_.roomId == lift(roomId))
        .filter(_.membership == lift(membership))
        .join(users)
        .on(_.userId == _.userId)
        .join(events)
        .on { case ((rm, _), e) => rm.eventId == e.id }
        .map { case ((_, user), event) => (user, event) }
    }
  }

  /**
    * Get a room last event, ie: the event with the max streamOrder value among room events
    * @param roomId
    * @return
    */
  def getRoomLastEvent(roomId: UUID): Try[Option[Event]] = Try {
    val maxStreamOrder: Long = run(events.filter(_.roomId == lift(roomId)).map(_.streamOrder).max).getOrElse(0L)
    run {
      //See https://github.com/getquill/quill/issues/1857 for a fix
      //val maxStreamOrder = events.filter(_.roomId == lift(roomId)).map(_.streamOrder).max.getOrElse(0L)
      events.filter(_.streamOrder == lift(maxStreamOrder))
    }.headOption
  }

  /**
    * Get the last room event before the given stream position
    * @param roomId
    * @return
    */
  def getRoomLastEventBeforeStreamPosition(roomId: UUID, streamOrderPosition: Long): Try[Option[Event]] = Try {
    val maxStreamOrder = run(
      events
        .filter(_.roomId == lift(roomId))
        .filter(_.streamOrder <= lift(streamOrderPosition))
        .map(_.streamOrder)
        .max
    ).getOrElse(0L)
    run {
      events.filter(_.streamOrder == lift(maxStreamOrder))
    }.headOption
  }

  /**
    * Query all room events having streamOrder > minStreamOrder and streamOrder <= maxStreamOrder
    * @param roomId
    * @param minStreamOder
    * @param maxStreamOrder
    * @return
    */
  def getRoomEvents(
      roomId: UUID,
      minStreamOder: Long = 0L,
      maxStreamOrder: Long = Long.MaxValue
  ): Try[List[Event]] = Try {
    run {
      events
        .filter(_.roomId == lift(roomId))
        .filter(_.streamOrder > lift(minStreamOder))
        .filter(_.streamOrder <= lift(maxStreamOrder))
        .sortBy(_.streamOrder)(Ord.asc)
    }
  }

}
