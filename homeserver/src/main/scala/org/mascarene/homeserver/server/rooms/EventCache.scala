/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import java.util.UUID

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import io.circe.generic.auto._
import org.mascarene.homeserver.server.model.{DbContext, Event, EventRepo}
import redis.clients.jedis.JedisPool
import scalacache._
import scalacache.modes.try_._
import scalacache.redis._
import scalacache.serialization.circe._

import scala.jdk.DurationConverters._
import scala.util.{Success, Try}

class EventCache(config: Config, dbContext: DbContext, jedisPool: JedisPool) extends LazyLogging {
  private[this] val eventRepo                                = new EventRepo(dbContext)
  private[this] val eventAuthEdgesCache: Cache[Set[Event]]   = RedisCache(jedisPool)
  private[this] val eventParentEdgesCache: Cache[Set[Event]] = RedisCache(jedisPool)
  private[this] val eventsCache: Cache[Event]                = RedisCache(jedisPool)
  private[this] val eventCacheTtl                            = Some(config.getDuration("mascarene.internal.cache-ttl.event").toScala)

  private def eventAuthEdgeKey(eventId: UUID)   = s"event($eventId).authEdges"
  private def eventParentEdgeKey(eventId: UUID) = s"event($eventId).parentEdges"
  private def eventKey(eventId: UUID)           = s"event($eventId)"

  def getEventById(eventId: UUID): Try[Option[Event]] = {
    eventsCache.get(eventKey(eventId)).orElse {
      val event = eventRepo.getEventById(eventId)
      event.foreach { case Some(e) => eventsCache.put(eventKey(eventId))(e, eventCacheTtl) }
      event
    }
  }

  def removeEvent(eventId: UUID): Unit = {
    eventsCache.remove(eventKey(eventId))
  }

  def putEvent(event: Event): Unit = {
    eventsCache.put(eventKey(event.id))(event, eventCacheTtl)
  }

  def getEventAuthEdges(eventId: UUID): Try[Set[Event]] = {
    eventAuthEdgesCache.get(eventAuthEdgeKey(eventId)).flatMap {
      case Some(result) =>
        Success(result)
      case None =>
        eventRepo.getAuthEvents(eventId).map { s =>
          eventAuthEdgesCache.put(eventAuthEdgeKey(eventId))(s, eventCacheTtl)
          s
        }
    }
  }

  def getEventParentEdges(eventId: UUID): Try[Set[Event]] = {
    eventParentEdgesCache.get(eventParentEdgeKey(eventId)).flatMap {
      case Some(result) =>
        Success(result)
      case None =>
        eventRepo.getParentEvents(eventId).map { s =>
          eventParentEdgesCache.put(eventParentEdgeKey(eventId))(s, eventCacheTtl)
          s
        }
    }
  }
}
