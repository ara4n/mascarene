/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import org.mascarene.homeserver.server.model.{AuthRepo, DbContext, Event, EventRejection, EventTypes, Room}
import org.mascarene.utils.{RoomIdentifierUtils, UserIdentifierUtils}
import redis.clients.jedis.JedisPool
import scalacache.{redis, _}
import scalacache.redis._
import scalacache.modes.try_._
import io.circe.generic.auto._
import scalacache.serialization.circe._
import cats.data.ValidatedNec
import cats.implicits._
import org.mascarene.utils.RoomIdentifierUtils

import scala.util.{Failure, Success, Try}

class V1RoomImpl(room: Room)(implicit config: Config, dbContext: DbContext, jedisPool: JedisPool)
    extends RoomVersionImpl
    with LazyLogging {

  protected[this] val authRepo = new AuthRepo(dbContext)

  /**
    * Authorize event according to https://matrix.org/docs/spec/rooms/v1#id3
    * @param resolvedEvent
    * @return
    */
  override def resolve(resolvedEvent: Event): (Event, Try[StateSet]) = {
    logger.error("V1 algorithm is obsolete, not implemented")
    throw new NotImplementedError("V1 algorithm is obsolete, not implemented")
  }
}

object V1RoomImpl {
  def apply(room: Room)(implicit config: Config, dbContext: DbContext, jedisPool: JedisPool) =
    new V1RoomImpl(room)
}
