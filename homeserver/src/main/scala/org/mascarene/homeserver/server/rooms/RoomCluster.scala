/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.server.rooms

import akka.actor.typed.scaladsl.{AbstractBehavior, ActorContext, Behaviors, Routers}
import akka.actor.typed.{ActorRef, Behavior, SupervisorStrategy}
import akka.cluster.sharding.typed.scaladsl.{ClusterSharding, Entity, EntityRef}
import com.typesafe.config.Config
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.server.model.{DbContext, Room}
import org.mascarene.homeserver.server.rooms.RoomServer.TypeKey
import redis.clients.jedis.JedisPool

import scala.concurrent.ExecutionContextExecutor

object RoomCluster {
  sealed trait Response
  case class GotRoomServer(serverRef: EntityRef[RoomServer.Command]) extends Response

  sealed trait Command
  case class GetRoomServer(room: Room, replyTo: ActorRef[GotRoomServer]) extends Command

  def apply(
      config: Config,
      dbContext: DbContext,
      jedisPool: JedisPool,
      eventStreamSequence: ActorRef[EventStreamSequence.Command]
  ): Behavior[RoomCluster.Command] =
    Behaviors.setup { ctx => new RoomCluster(ctx, config, dbContext, jedisPool, eventStreamSequence) }
}

class RoomCluster(
    context: ActorContext[RoomCluster.Command],
    val config: Config,
    dbContext: DbContext,
    jedisPool: JedisPool,
    eventStreamSequence: ActorRef[EventStreamSequence.Command]
) extends AbstractBehavior[RoomCluster.Command](context)
    with ImplicitAskTimeOut {
  implicit val ec: ExecutionContextExecutor = context.executionContext
  private val sharding                      = ClusterSharding(context.system)

  private val workerPoolSize = config.getInt("mascarene.internal.server.room.worker-pool-size")
  private val pool = Routers.pool(workerPoolSize)(
    Behaviors.supervise(RoomWorker(config, dbContext, jedisPool)).onFailure[Exception](SupervisorStrategy.restart)
  )
  private val workerRouter = context.spawn(pool, "room-worker-pool")

  sharding.init(Entity(TypeKey) { entityContext =>
    RoomServer(entityContext.entityId, config, dbContext, jedisPool, eventStreamSequence, workerRouter)
  })

  import RoomCluster._
  override def onMessage(msg: Command): Behavior[Command] = {
    msg match {
      case GetRoomServer(room, replyTo) =>
        replyTo ! GotRoomServer(sharding.entityRefFor(RoomServer.TypeKey, room.id.toString))
        this
    }
  }
}
