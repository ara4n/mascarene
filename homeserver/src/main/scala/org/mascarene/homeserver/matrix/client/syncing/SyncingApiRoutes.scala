/*
 * Mascarene
 * Copyright (C) 2020  mascarene.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.mascarene.homeserver.matrix.client.syncing

import java.time.ZoneId
import java.util.concurrent.TimeUnit

import scala.util.chaining._
import akka.actor.typed.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import com.typesafe.config.Config
import com.typesafe.scalalogging.LazyLogging
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport
import io.circe.generic.auto._
import io.circe.{Json, Printer}
import org.mascarene.homeserver.server.model.{AccountData, DbContext, Event, Room}
import org.mascarene.homeserver.ImplicitAskTimeOut
import org.mascarene.homeserver.matrix.client.AccountDataApi
import org.mascarene.homeserver.matrix.{ApiErrorRejection, ApiErrors}
import org.mascarene.homeserver.matrix.client.auth.{AuthApi, AuthCredentials, AuthDirectives}
import org.mascarene.homeserver.matrix.client.filtering.FilteringApi
import org.mascarene.homeserver.server.rooms.{EventApi, RoomApi, StateSet}
import org.mascarene.homeserver.server.users.UserApi
import org.mascarene.matrix.client.r0.model.events.{
  AccountDataEvent,
  InviteState,
  InvitedRoom,
  JoinedRoom,
  LeftRoom,
  Rooms,
  State,
  StrippedEvent,
  SyncResponse,
  Timeline,
  AccountData => ApiAccountData
}
import org.mascarene.matrix.client.r0.model.filter.FilterDefinition

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future, TimeoutException}
import scala.jdk.DurationConverters._

class SyncingApiRoutes(
    val config: Config,
    dbContext: DbContext,
    implicit val system: ActorSystem[_]
) extends FailFastCirceSupport
    with AuthDirectives
    with ImplicitAskTimeOut
    with LazyLogging {
  implicit val printer: Printer = Printer.noSpaces.copy(dropNullValues = true)
  implicit val ec               = system.executionContext

  protected val authApi      = new AuthApi(config, dbContext, system)
  private val filteringApi   = new FilteringApi(config, dbContext)
  private val accountDataApi = new AccountDataApi(dbContext)
  private val userApi        = new UserApi(dbContext)
  private val roomApi        = new RoomApi(config, dbContext, system)
  private val eventApi       = new EventApi(config, dbContext)

  private val syncMaxTimeout = config.getDuration("mascarene.server.client.sync-max-timeout").toScala

  lazy val routes: Route = pathPrefix("_matrix" / "client" / "r0") {
    path("sync") {
      get {
        requireAuth { credentials =>
          withRequestTimeout(syncMaxTimeout) {
            parameters(
              "filter".as[String].?,
              "since".as[Long].?,
              "full_state".as[Boolean].?,
              "set_presence".as[String].?,
              "timeout".as[Long].?
            ) {
              case (
                  mayBeFilter: Option[String],
                  mayBeSince: Option[Long],
                  mayBeFullState: Option[Boolean],
                  mayBeSetPresence: Option[String],
                  mayBeTimeout: Option[Long]
                  ) =>
                logger.info(
                  s"API CALL - get sync with :filter=$mayBeFilter, since=$mayBeSince, full_state=$mayBeFullState, set_presence=$mayBeSetPresence, timeout=$mayBeTimeout"
                )
                val fullState     = mayBeFullState.getOrElse(false)
                val timeout: Long = mayBeTimeout.getOrElse(0L)
                val timeoutFuture = Future { Thread.sleep(timeout) }
                val since         = mayBeSince.getOrElse(Long.MinValue)

                val filterDefinition =
                  filteringApi.parseOrGetFilterDefinition(mayBeFilter.getOrElse(Json.Null.noSpaces))
                val setPresence = mayBeSetPresence.getOrElse("online")
                if (filterDefinition.isFailure)
                  reject(
                    ApiErrorRejection(
                      ApiErrors.BadJson(Some("Filter definition can't be parsed from JSON or is an invalid filter ID"))
                    )
                  )
                else if (!Seq("offline", "online", "unavailable").contains(setPresence)) {
                  reject(
                    ApiErrorRejection(
                      ApiErrors.Unrecognized(Some(s"Unrecognized set_presence value '$setPresence'"))
                    )
                  )
                } else {
                  //All parameters checked !
                  val accountDataFuture = Future.fromTry {
                    accountDataApi.getAccountData(credentials.account).map { list =>
                      if (list.isEmpty) None else Some(accountDataToApiModel(list))
                    }
                  }
                  val syncRoomFuture = syncRooms(credentials, filterDefinition.get, fullState, mayBeSince)

                  onComplete(Future.sequence(List(accountDataFuture, syncRoomFuture))) { _ =>
                    val accountDataEvents           = Await.result(accountDataFuture, Duration.MinusInf)
                    val (rooms, syncMaxStreamOrder) = Await.result(syncRoomFuture, Duration.MinusInf)
                    val nextBatch                   = if (syncMaxStreamOrder <= since) since else syncMaxStreamOrder
                    var response                    = SyncResponse(nextBatch.toString)

                    if (accountDataEvents.nonEmpty || nextBatch > since) {
                      response = response.copy(account_data = accountDataEvents, rooms = Some(rooms))
                      complete(response)
                    } else {
                      complete(timeoutFuture.map(_ => response))
                    }
                  }
                }
              case _ =>
                reject(
                  ApiErrorRejection(
                    ApiErrors.Unrecognized(Some(s"Invalid query parameters"))
                  )
                )
            }
          }
        }
      }
    }
  }

  /**
    * Compute /sync API rooms updates
    *
    * @param credentials      current user credentials
    * @param filterDefinition filter definition used to filter results
    * @param fullState        requires full state N
    * @param mayBeSince       state update begining (as stream order position)
    * @return
    */
  private def syncRooms(
      credentials: AuthCredentials,
      filterDefinition: FilterDefinition,
      fullState: Boolean,
      mayBeSince: Option[Long] = None
  ): Future[(Rooms, Long)] = {
    val futureResults = for {
      roomMemberships <- userApi.getRoomMemberships(credentials.user)
      roomLastEvents  <- getRoomsLastEvent(roomMemberships.map(_._1).toList)
      roomStateSets   <- getRoomsStateSetAtEvent(roomLastEvents)
      nextBatch <- Future.successful(
        roomLastEvents.values.filter(_.isDefined).map(_.get.streamOrder).minOption.getOrElse(Long.MinValue)
      )
      diffs <- diffRoomStateSets(roomMemberships.map(_._1).toList, mayBeSince.getOrElse(nextBatch), nextBatch)
    } yield (roomMemberships, roomStateSets, nextBatch, diffs)

    futureResults.flatMap {
      case (roomMemberships, roomStateSets, nextBatch, diffs) =>
        val since       = mayBeSince.getOrElse(nextBatch)
        val joinRooms   = roomMemberships.filter(_._2 == "join").map(_._1)
        val inviteRooms = roomMemberships.filter(_._2 == "invite").map(_._1)
        val leftRooms   = roomMemberships.filter(_._2 == "leave").map(_._1)

        // Fill JoinedRoom
        val joinedRoomsIterableFutures: Iterable[Future[(String, JoinedRoom)]] = joinRooms.map { room =>
          val roomSummaryFuture =
            Future.fromTry(roomApi.getRoomSummary(room, nextBatch, Some(credentials.user.mxUserId)))
          val modelApiStateSetFuture = stateSetToApiModel {
            if (fullState)
              roomStateSets.getOrElse(room, StateSet())
            else
              diffs.getOrElse(room, StateSet())
          }
          val eventsTimelineFuture   = eventsTimeline(room, since, nextBatch)
          val roomsAccountDataFuture = Future.fromTry(accountDataApi.getAccountData(credentials.account, room))
          //ephemeral and unread_notifications are note yet implemented

          val joinRoomFuture = for {
            roomSummary      <- roomSummaryFuture
            modelApiStateSet <- modelApiStateSetFuture
            eventsTimeline   <- eventsTimelineFuture
            roomAccountData  <- roomsAccountDataFuture
          } yield (roomSummary, modelApiStateSet, eventsTimeline, roomAccountData)

          joinRoomFuture.map {
            case (roomSummary, modelApiStateSet, eventsTimeline, roomAccountData) =>
              val accountData = if (roomAccountData.isEmpty) None else Some(accountDataToApiModel(roomAccountData))
              room.mxRoomId -> JoinedRoom(
                Some(roomSummary),
                Some(modelApiStateSet),
                Some(eventsTimeline),
                None,
                accountData,
                None
              )
          }
        }
        val joinedRoomsFutures =
          Future.foldLeft(joinedRoomsIterableFutures.toList)(Map.empty[String, JoinedRoom])((acc, elem) =>
            acc.updated(elem._1, elem._2)
          )

        //Fill InvitedRoom
        val inviteRoomIterableFutures = inviteRooms.map { room =>
          val memberShipEvent = roomMemberships.find(_._1 == room).map(_._3).get
          for {
            stateSet    <- Future.fromTry(roomApi.getRoomStateAtEvent(room, memberShipEvent))
            inviteState <- stateSetToInvitedRoomApiModel(stateSet)
          } yield room.mxRoomId -> InvitedRoom(inviteState)
        }
        val invitedRoomsFutures =
          Future.foldLeft(inviteRoomIterableFutures.toList)(Map.empty[String, InvitedRoom])((acc, elem) =>
            acc.updated(elem._1, elem._2)
          )

        //Fill LeftRoom
        val leftRoomIterableFutures = leftRooms.map { room =>
          val modelApiStateSetFuture = stateSetToApiModel {
            if (fullState)
              roomStateSets.getOrElse(room, StateSet())
            else
              diffs.getOrElse(room, StateSet())
          }
          val eventsTimelineFuture   = eventsTimeline(room, since, nextBatch)
          val roomsAccountDataFuture = Future.fromTry(accountDataApi.getAccountData(credentials.account, room))
          //ephemeral and unread_notifications are note yet implemented

          val leftRoomFuture = for {
            modelApiStateSet <- modelApiStateSetFuture
            eventsTimeline   <- eventsTimelineFuture
            roomAccountData  <- roomsAccountDataFuture
          } yield (modelApiStateSet, eventsTimeline, roomAccountData)

          leftRoomFuture.map {
            case (modelApiStateSet, eventsTimeline, roomAccountData) =>
              val accountData = if (roomAccountData.isEmpty) None else Some(accountDataToApiModel(roomAccountData))
              room.mxRoomId -> LeftRoom(modelApiStateSet, eventsTimeline, accountData)
          }
        }
        val leftRoomsFutures =
          Future.foldLeft(leftRoomIterableFutures.toList)(Map.empty[String, LeftRoom])((acc, elem) =>
            acc.updated(elem._1, elem._2)
          )

        val roomsFuture = for {
          joinedRoom  <- joinedRoomsFutures
          invitedRoom <- invitedRoomsFutures
          leftRoom    <- leftRoomsFutures
        } yield (joinedRoom, invitedRoom, leftRoom)

        roomsFuture.map {
          case (join, invite, left) =>
            (Rooms(join = join, invite = invite, left), nextBatch)
        }
    }
  }

  private def getRoomsLastEvent(rooms: List[Room]): Future[Map[Room, Option[Event]]] =
    Future.foldLeft {
      rooms.map(room => Future.fromTry(roomApi.getRoomLastEvent(room).map(lastEvent => (room, lastEvent))))
    }(
      Map.empty[Room, Option[Event]]
    )((acc, elem) => acc.updated(elem._1, elem._2))

  private def getRoomsStateSetAtEvent(roomsMap: Map[Room, Option[Event]]): Future[Map[Room, StateSet]] = {
    Future
      .sequence {
        roomsMap.map {
          case (room, lastEvent) =>
            lastEvent
              .map(e =>
                Future
                  .fromTry(roomApi.getRoomStateAtEvent(room, e))
              )
              .getOrElse(Future.successful(StateSet()))
              .map(stateSet => room -> stateSet)
        }
      }
      .map(_.toMap)
  }

  private def diffRoomStateSets(rooms: List[Room], since: Long, to: Long): Future[Map[Room, StateSet]] = Future {
    rooms.map { room => room -> roomApi.diffRoomStateSet(room, since, to).get }.toMap
  }

  private def stateSetToApiModel(stateSet: StateSet): Future[State] = {
    val eventList: List[Event] = stateSet.toList.map(_._3)
    val contentsFuture         = Future.fromTry(eventApi.getEventsContents(eventList))
    val sendersFuture          = Future.fromTry(eventApi.getEventsSenders(eventList))
    val apiFuture = for {
      contents <- contentsFuture
      senders  <- sendersFuture
    } yield (contents, senders)

    apiFuture.map {
      case (contents, senders) =>
        eventList
          .map { event =>
            val content = contents(event)
            val sender  = senders(event)
            org.mascarene.matrix.client.r0.model.events.StateEvent(
              content.content.getOrElse(Json.Null),
              event.eventType,
              event.mxEventId,
              sender.mxUserId,
              event.originServerTs.atZone(ZoneId.systemDefault()).toInstant.getEpochSecond,
              event.unsigned,
              Json.Null,
              event.stateKey.get
            )
          }
          .pipe(State.apply)
    }
  }

  private def accountDataToApiModel(accountDataList: List[AccountData]): ApiAccountData = {
    accountDataList.map { ac => AccountDataEvent(ac.eventType, ac.eventContent) }.pipe(ApiAccountData.apply)
  }

  private def stateSetToInvitedRoomApiModel(stateSet: StateSet): Future[InviteState] = {
    val eventList: List[Event] = stateSet.toList.map(_._3)
    val contentsFuture         = Future.fromTry(eventApi.getEventsContents(eventList))
    val sendersFuture          = Future.fromTry(eventApi.getEventsSenders(eventList))
    val apiFuture = for {
      contents <- contentsFuture
      senders  <- sendersFuture
    } yield (contents, senders)

    apiFuture.map {
      case (contents, senders) =>
        eventList
          .map { event =>
            val content = contents(event)
            val sender  = senders(event)
            StrippedEvent(content.content.getOrElse(Json.Null), event.stateKey.get, event.eventType, sender.mxUserId)
          }
          .pipe(InviteState.apply)
    }
  }

  private def eventsTimeline(room: Room, minStreamOder: Long, maxStreamOrder: Long): Future[Timeline] = {
    val apiFuture = for {
      events   <- Future.fromTry(roomApi.getRoomEvents(room, minStreamOder, maxStreamOrder))
      contents <- Future.fromTry(eventApi.getEventsContents(events))
      senders  <- Future.fromTry(eventApi.getEventsSenders(events))
    } yield (events, contents, senders)
    apiFuture.map {
      case (events, contents, senders) =>
        events
          .map { event =>
            val content = contents(event)
            val sender  = senders(event)
            org.mascarene.matrix.client.r0.model.events.TimeLineEvent(
              content.content.getOrElse(Json.Null),
              event.eventType,
              event.mxEventId,
              sender.mxUserId,
              event.originServerTs.atZone(ZoneId.systemDefault()).toInstant.getEpochSecond,
              event.unsigned
            )
          }
          .pipe(list => Timeline.apply(list, None, None))
    }
  }
}
